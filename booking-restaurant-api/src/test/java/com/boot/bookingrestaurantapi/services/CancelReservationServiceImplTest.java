package com.boot.bookingrestaurantapi.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.boot.bookingrestaurantapi.entities.Reservation;
import com.boot.bookingrestaurantapi.exceptions.BookingException;
import com.boot.bookingrestaurantapi.repositories.ReservationRespository;
import com.boot.bookingrestaurantapi.services.impl.CancelReservationServiceImpl;


public class CancelReservationServiceImplTest {

	private static final String LOCATOR = "Burguer 3";
	private static final Reservation RESERVATION = new Reservation();
	private static final String RESERVATION_DELETED = "LOCATOR_DELETED";
	
	@Mock
	private ReservationRespository reservationRespository;
	
	@InjectMocks
	private CancelReservationServiceImpl cancelReservationServiceImpl;
	
	@Before
	public void init() throws BookingException {
		MockitoAnnotations.initMocks(this);
		
	
	}
	
	@Test
	public void deleteReservationTest()throws BookingException{
	Mockito.when(reservationRespository.findByLocator(LOCATOR)).thenReturn(Optional.of(RESERVATION));
	Mockito.when(reservationRespository.deleteByLocator(LOCATOR)).thenReturn(Optional.of(RESERVATION));
	final String response = cancelReservationServiceImpl.deleteReservation(LOCATOR);
	assertEquals(response,RESERVATION_DELETED );
	}
	
	@Test(expected = BookingException.class)
	public void deleteReservationfindByLocatorTest() throws BookingException {
		Mockito.when(reservationRespository.findByLocator(LOCATOR)).thenReturn(Optional.empty());
		cancelReservationServiceImpl.deleteReservation(LOCATOR);
		fail();
	}
	@Test(expected = BookingException.class)
	public void deleteReservationTestError() throws BookingException {
		Mockito.when(reservationRespository.findByLocator(LOCATOR)).thenReturn(Optional.of(RESERVATION));
		Mockito.doThrow(Exception.class).when(reservationRespository).deleteByLocator(LOCATOR);
		cancelReservationServiceImpl.deleteReservation(LOCATOR);
		fail();
	}
	
	
}
